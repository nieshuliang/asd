const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 解决跨域
  devServer:{
    proxy:"http://admin.raz-kid.cn",
  }
})
