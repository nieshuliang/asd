import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from "nprogress";
import "nprogress/nprogress.css";
Vue.use(VueRouter)
const routes = [
  {
    path: '/home',
    redirect:{name:"Nick"},
    meta: {
      title: "首页",
    },
    name: 'Home',
    component: () => import(/* webpackChunkName: "Home" */ '../views/Home/Home.vue'),
    children: [
      {
        path: "/Nick",
        name: "Nick",
        component: () =>import(/* webpackChunkName: "Nick" */ "../views/Home/Nick.vue"),
        meta: {
          title: "首页",
        },
      },
      {
        path: "/goods",
        name: "goods",
        meta: {
          title: "商品",
        },
        component: () =>
       import(/* webpackChunkName: "Goods" */ "../views/Goods/goods.vue"), 
       children:[
        {
          path: "/list",
          name: "list",
          meta: {
            title: "商品管理",
          },
          component: () =>
         import(/* webpackChunkName: "Goods" */ "../views/Goods/list.vue"), 
        },
        {
          path: "/add",
          name: "add",
          meta: {
            title: "新增商品",
          },
          component: () =>
            import(
              /* webpackChunkName: "Goods" */ "@/views/Goods/add.vue"
            ),
        },
        {
          path: "/edit",
          name: "edit",
          meta: {
            title: "编辑商品",
          },
          props: true,
          component: () =>
            import(
              /* webpackChunkName: "Goods" */ "@/views/Goods/edit.vue"
            ),
        },
       ]
      },
      {
        path: "/category",
        name: "category",
        meta: {
          title: "品类",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () =>
          import(/* webpackChunkName: "Category" */ "@/views/Category/Category.vue"),
        children: [
          {
            path: "/category/list/:id?",
            alias: "/category/list/:id?",
            name: "categoryList",
            meta: {
              title: "品种分类",
            },
            props: true,
            component: () =>
              import(
                /* webpackChunkName: "Category" */ "@/views/Category/Category.vue"
              ),
          },
        ],
      },
      {
        path: "/orders",
        name: "orders",
        meta: {
          title: "订单",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () =>
          import(/* webpackChunkName: "Order" */ "@/views/Orders/Orders"),
        children: [
          {
            path: "/orders/list/:id?",
            alias: "/orders/list/:id?",
            name: "orderList",
            meta: {
              title: "订单列表",
            },
            props: true,
            component: () =>
              import(/* webpackChunkName: "Order" */ "@/views/Orders/Orders.vue"),
          },
        ],
      },
      {
        path: "/user",
        name: "user",
        meta: {
          title: "用户",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () => import(/* webpackChunkName: "User" */ "@/views/User/User"),
        children: [
          {
            path: "/user/list",
            alias: "/user/list",
            name: "userList",
            meta: {
              title: "用户列表",
            },
            props: true,
            component: () =>
              import(/* webpackChunkName: "User" */ "@/views/User/User.vue"),
          },
        ],
      },
    ],
  },
  {
    path: '/',
    name: 'Login',
    component: () => import(/* webpackChunkName: "Login" */ '../views/Login/index.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
