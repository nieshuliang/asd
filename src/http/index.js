import axios from "axios";
import router from "@/router/index";
import {Message,Loading} from "element-ui"
import qs from "qs"

// 添加请求拦截器
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";
axios.interceptors.request.use(
  function (config) {
   
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (res) {
    // 对响应数据做点什么
    if (res.status >= 200 && res.status < 300) {
      if (res.data.status === 0) {
        return Promise.resolve(res.data);
      } else if (res.data.status === 10) {
        Message.error(res.data);
        setTimeout(() => {
          router.push({
            name: "login",
            query: {
              redirect: router.currentRoute.fullPath,
            },
          });
        }, 1500);
      } else {
        Message.error(res.data.msg);
        return Promise.reject(res.data.msg);
      }
    } else {
      Message.warning(res.statusText);
      return Promise.reject(res.statusText);
    }
    return res;
  },
  function (error) {
    // 对响应错误做点什么
    Message.error(error);
    return Promise.reject(error);
  }
);
function http(url, method = "GET", data = {}, params = {}, loading) {
  let _data = {},
    _loading;
  if (
    Object.prototype.toString.call(data) === "[object Object]" &&
    Object.keys(data).length > 0
  ) {
    _data = new URLSearchParams();
    Object.keys(data).map((key) => {
      _data.append(key, data[key]);
    });
  }
  if (loading) {
    this[loading] = true;
  } else {
    _loading = Loading.service({
      lock: true,
      text: "Loading",
      spinner: "el-icon-loading",
      background: "rgba(0, 0, 0, 0.7)",
    });
  }
  return axios({
    url,
    method,
    data: _data,
    params,
    loading,
  }).finally(() => {
    loading && (this[loading] = false);
    _loading && _loading.close();
  });
}
// 设置请求头
let instance = axios.create({
  baseURL: "",
  //设置超时时间
  timeout: 5000,
  headers: {
    post: {
      "Content-type": "application/x-www-form-urlencoded",
    },
  },
});
function post(url, data = {}) {
  return instance
    .post(url, new URLSearchParams(qs.stringify(data)))
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (res.data.status === 0) {
          return res.data;
        } else {
          Message.err(res.data.msg);
          return Promise.reject(res.data.msg);
        }
      } else {
        Message.err(res.statusText);
        return Promise.reject(res.statusText);
      }
    })
    .catch((err) => {
      Promise.reject(err);
    });
}
function get(url, params = {}) {
  return instance
    .get(url, {
      params,
    })
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (res.data.status === 0) {
          return res.data;
        } else if (res.data.status === 10) {
          Message.error(res.data.msg);
          router.replace({
            name: "login",
            query: {
              redirect: router.currentRoute.fullPath,
            },
          });
        } else {
          Message.error(res.data.msg);
          Promise.reject(res.data.msg);
        }
      } else {
        Promise.reject(res.statusText);
      }
    })
    .catch((err) => {
      Promise.reject(err);
    });
}
// function get(url, params = {}) {
//   return instance
//     .get(url, {
//       params,
//     })
//     .then((res) => {
//       if (res.status >= 200 && res.status < 300) {
//         if (res.data.status === 0) {
//           return res.data;
//         } else if (res.data.status === 10) {
//           Message.error(res.data.msg);
//           router.replace({
//             name: "login",
//             query: {
//               redirect: router.currentRoute.fullPath,
//             },
//           });
//         } else {
//           Promise.reject(res.data.msg);
//         }
//       } else {
//         Promise.reject(res.statusText);
//       }
//     })
//     .catch((err) => {
//       Promise.reject(err);
//     });
// }
// function upload(url, data = {}, config) {
//   const obj = new FormData();
//   Object.keys(data).map((item) => {
//     obj.append(item, data[item]);
//   });
//   return formData
//     .post(url, obj, config)
//     .then((res) => {
//       if (res.status >= 200 && res.status < 300) {
//         if (res.data.code === 0) {
//           return res.data;
//         } else {
//           Message.err(res.data.msg);
//           return Promise.reject(res.data.msg);
//         }
//       } else {
//         Message.err(res.statusText);
//         return Promise.reject(res.statusText);
//       }
//     })
//     .catch((err) => {
//       Promise.reject(err);
//     });
// }
function upload(url, data = {}, config) {
  const obj = new FormData();
  Object.keys(data).map((item) => {
    obj.append(item, data[item]);
  });
  return formData
    .post(url, obj, config)
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (res.data.code === 0) {
          return res.data;
        } else {
          Message.error(res.data.msg);
          return Promise.reject(res.data.msg);
        }
      } else {
        Message.error(res.statusText);
        return Promise.reject(res.statusText);
      }
    })
    .catch((err) => {
      Promise.reject(err);
    });
}
export default http;
export {post,get,upload};