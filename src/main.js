import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'normalize.css'
import RouterTab from "vue-router-tab";
// 全屏插件
import VueFullscreen from 'vue-fullscreen';
import axios from "axios"
import App from './App.vue'
import router from './router'
import store from './store'
Vue.use(ElementUI);
Vue.use(RouterTab);
// 全屏插件
Vue.use(VueFullscreen)
Vue.config.productionTip = false
Vue.prototype.$http=axios
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
